package automatizacion.nivel1;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Funciones {
	//-----------------------Clase 1-----------------------	
	public static void ObtenerTitulo() {
		String Titulo=Driver.driver.getTitle();
		System.out.println(Titulo);
	}
	
	public static void IngresarUrl(String url) {
		Driver.driver.get(url);
	}
	
	public static void ObtenerUrl() {
		String Url=Driver.driver.getCurrentUrl();
		System.out.println(Url);
	}
	
	public static void ObtenerSource() {
		String Source=Driver.driver.getPageSource();
		System.out.println(Source);
	}
	
	public static void Navegar(String url) {
		Driver.driver.navigate().to(url);
	}
	
	public static void NavegarAtras() {
		Driver.driver.navigate().back();
	}
	
	public static void NavegarAdelante() {
		Driver.driver.navigate().forward();
	}
	
	public static void Actualizar() {
		Driver.driver.navigate().refresh();
	}
//-----------------------Clase 2-----------------------
	public static void Limpiar() {
		Variables.objeto.clear();
	}
	
	public static void Click() {
		Variables.objeto.click();
	}
	
	public static void Escribir(String Texto) {
		Variables.objeto.sendKeys(Texto);
	}
	
	public static void LeerTexto() {
		String Texto = Variables.objeto.getText();
		System.out.println(Texto);
	}
	
	public static void LeerAtributo(String atributo) {
		String Atributo = Variables.objeto.getAttribute(atributo);
		System.out.println(Atributo);
	}
	
	public static void Visualiza() {
		boolean Visualiza = Variables.objeto.isDisplayed();
		System.out.println(Visualiza);
	}
	
	public static void Habilitado() {
		boolean Habilitado = Variables.objeto.isEnabled();
		System.out.println(Habilitado);
	}
	
	public static void SeleccionarTexto(String Texto) {
		Variables.objeto_Select.selectByVisibleText(Texto);
	}
	
	public static void SeleccionarIndex(int Index) {
		Variables.objeto_Select.selectByIndex(Index);
	}
	
	public static void SeleccionarValue(String Value) {
		Variables.objeto_Select.selectByValue(Value);
	}
//-----------------------Clase 3-----------------------	
	
	public static void IngresarIframe(String Nombre) {
		Driver.driver.switchTo().frame(Nombre);
	}
	
	public static void SalirIframe() {
		Driver.driver.switchTo().defaultContent();
	}
	
	public static void AceptarDialogo() {
		Variables.alert = Driver.driver.switchTo().alert();
		Variables.alert.accept();
	}
	
	public static void CancelarDialogo() {
		Variables.alert = Driver.driver.switchTo().alert();
		Variables.alert.dismiss();
	}
	
	public static void EscribirDialogo(String Texto) {
		Variables.alert = Driver.driver.switchTo().alert();
		Variables.alert.sendKeys(Texto);
		Variables.alert.accept();
	}
	
	public static void LeerDialogo() {
		Variables.alert = Driver.driver.switchTo().alert();
		String mensaje = Variables.alert.getText();
		System.out.println(mensaje);
		Variables.alert.accept();
	}
	
	public static void window(){
        Set<String> handles = Driver.driver.getWindowHandles();
        for( String handle: handles) {
        	Driver.driver.switchTo().window(handle);
        } 
	}
	
	public static void DragAndDrop(WebElement draggable, WebElement droppable) {
		Variables.actions = new Actions (Driver.driver);
		Variables.actions.dragAndDrop(draggable,droppable)
			.perform();
	}
	
	public static void SeleccionMultiple(WebElement Item1, WebElement Item2) {
		Variables.actions = new Actions (Driver.driver);
		Variables.actions.clickAndHold(Item1)
	        .moveToElement(Item2)
	        .release()
	        .perform();
	}
	
	public static void DobleClick(WebElement objeto) {
		Variables.actions = new Actions(Driver.driver); 
		Variables.actions.doubleClick(objeto)
		.perform(); 
	}
	
	public static void TeclaEnter() {
		Variables.objeto.sendKeys(Keys.ENTER);
	}
	
//-----------------------Clase 4-----------------------	
	
	public static void TiempoImplicito(int Tiempo) {
		Driver.driver.manage().timeouts().implicitlyWait(Tiempo, TimeUnit.SECONDS);
	}
	
	public static void TiempoExplicito(String Tipo, String objeto, int Tiempo) {
		WebDriverWait Tiempoespera = new WebDriverWait(Driver.driver,Tiempo);
		Tiempoespera.until(ExpectedConditions.presenceOfElementLocated(By.id(objeto)));
	}
	
	public static void Digitar(String Texto) {
		try {
			Variables.objeto.clear();
			Variables.objeto.click();
			String[] dato= Texto.split("");
		    for(int i = 0 ; i < dato.length ; i++){
		    	Variables.objeto.sendKeys(dato[i]);
		    }
		    
		    String descPaso = "Se ingreso correctamente el valor: "+Texto;
		    System.out.println(descPaso);
		    
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error en la función Digitar");
		}
	    
	}
	
}
