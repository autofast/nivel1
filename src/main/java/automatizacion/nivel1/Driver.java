package automatizacion.nivel1;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;


public class Driver {
	
	public static WebDriver driver;
	
	public static void AbrirNavegador(String Navegador) {

			switch (Navegador) {
			case "firefox":
				System.setProperty("webdriver.gecko.driver", "C:\\geckodriver.exe");
				driver = new FirefoxDriver();	
				driver.manage().window().maximize();
			break;
			
			case "chrome":
				System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
				driver= new ChromeDriver();
				driver.manage().window().maximize();
			break;
			} 
	}
}
