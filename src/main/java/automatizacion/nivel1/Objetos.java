package automatizacion.nivel1;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;


public class Objetos {
	
	public static void Identificador () {
		
		Variables.objeto = Driver.driver.findElement(By.id(""));
		Variables.objeto = Driver.driver.findElement(By.name(""));
		Variables.objeto = Driver.driver.findElement(By.tagName(""));
		Variables.objeto = Driver.driver.findElement(By.className(""));
		Variables.objeto = Driver.driver.findElement(By.linkText(""));
		Variables.objeto = Driver.driver.findElement(By.partialLinkText(""));
		Variables.objeto = Driver.driver.findElement(By.cssSelector(""));
		Variables.objeto = Driver.driver.findElement(By.xpath(""));
		
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.id("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.name("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.tagName("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.className("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.linkText("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.partialLinkText("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.cssSelector("")));
		Variables.objeto_Select = new Select(Driver.driver.findElement(By.xpath("")));
	}
	
	public static void Identificadores () {
		
		Variables.objetos = Driver.driver.findElements(By.id(""));
		Variables.objetos = Driver.driver.findElements(By.name(""));
		Variables.objetos = Driver.driver.findElements(By.tagName(""));
		Variables.objetos = Driver.driver.findElements(By.className(""));
		Variables.objetos = Driver.driver.findElements(By.linkText(""));
		Variables.objetos = Driver.driver.findElements(By.partialLinkText(""));
		Variables.objetos = Driver.driver.findElements(By.cssSelector(""));
		Variables.objetos = Driver.driver.findElements(By.xpath(""));
	}

}
